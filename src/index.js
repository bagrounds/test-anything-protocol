/**
 *
 * @module test-anything-protocol
 */
;(function () {
  'use strict'

  /* exports */
  module.exports = {
    plan: plan,
    test: test,
    diagnostic: diagnostic,
    bail: bail
  }

  function plan (number) {
    return '1..' + number
  }

  function test (options) {
    return (options.ok ? 'ok' : 'not ok') +
      (options.number ? ' ' + options.number : '') +
      (options.description ? ' - ' + options.description : '')
  }

  function diagnostic (comment) {
    return '# ' + comment
  }

  function bail () {
    return 'Bail out!'
  }
})()

